const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();
const PORT = process.env.PORT || 8080;

const {NodeCourseError} = require('./src/utils/errors');
const {authRouter} = require('./src/controllers/authController');
const {usersRouter} = require('./src/controllers/usersController');
const {truckRouter} = require('./src/controllers/trucksController');
const {loadRouter} = require('./src/controllers/loadController');
const {authMiddleware} = require('./src/middlewares/authMiddleware');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use('/api/users/me/', [authMiddleware], usersRouter);
app.use('/api/trucks/', [authMiddleware], truckRouter);
app.use('/api/loads/', [authMiddleware], loadRouter);

app.use((req, res, next) => {
  res.status(404).json({message: 'Not found'});
});

app.use((err, req, res, next) => {
  if (err instanceof NodeCourseError) {
    return res.status(err.status).json({message: err.message});
  }
  res.status(500).json({message: err.message});
});

const start = async () => {
  try {
    await mongoose.connect('mongodb+srv://Haldey:01280042@cluster.fefns.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false,
    });
    app.listen(PORT);
  } catch (err) {
    console.error(err.message);
  }
};

start();
