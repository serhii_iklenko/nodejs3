class NodeCourseError extends Error {
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

class InvalidRoleError extends NodeCourseError {
  constructor(message) {
    super(message);
    this.status = 403;
  }
}

class InvalidRequestError extends NodeCourseError {
  constructor(message = 'Invalid request') {
    super(message);
    this.status = 400;
  }
}

class CredentialsError extends NodeCourseError {
  constructor(message) {
    super(message);
    this.status = 401;
  }
}

class InvalidTypeError extends NodeCourseError {
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  NodeCourseError,
  InvalidRequestError,
  InvalidRoleError,
  CredentialsError,
  InvalidTypeError,
};
