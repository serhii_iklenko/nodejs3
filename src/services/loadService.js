const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');

const getUserLoads = async (userId, offset, limit) => {
  const loads = await Load.find({created_by: userId}, '-__v')
      .skip(offset)
      .limit(limit);

  return loads;
};

const addLoad = async (userId, data) => {
  const load = new Load({
    'name': data.name,
    'payload': data.payload,
    'pickup_address': data.pickup_address,
    'delivery_address': data.delivery_address,
    'dimensions': data.dimensions,
    'created_by': userId,
  });

  await load.save();
};

const getActiveLoads = async (assigned_to) => {
  const load = await Load.findOne({ assigned_to, status: 'ASSIGNED' })
  return load;
};

const setLoadState = async (assigned_to, state) => {
  if(state === 'Arrived to delivery') {
    await Load.findOneAndUpdate({ assigned_to, status: 'ASSIGNED' }, { state: state, status: 'SHIPPED', logs: { message: `Load state changed to '${state}'`, time: Date.now() } })
    await Truck.findOneAndUpdate({ assigned_to }, { status: 'IS' })
  } else {
    await Load.findOneAndUpdate({ assigned_to, status: 'ASSIGNED' }, { state: state, logs: { message: `Load state changed to '${state}'`, time: Date.now() } })
  }
};

const getLoadById = async (loadId, userId) => {
  const load = await Load.findOne({_id: loadId}, {created_by: userId}, '-__v');
  return load;
};

const updateLoadById = async (id, userId, data) => {
  const load = await Load.findOneAndUpdate({
    _id: id,
    created_by: userId,
    status: 'NEW',
  }, {$set: {data}});
  await load.save();
};

const deleteLoadById = async (id, userId) => {
  const load = await Load.findOneAndDelete({
    _id: id,
    created_by: userId,
    status: 'NEW',
  });
  return load;
};

const postLoadById = async (id, userId) => {
  const load = await Load.findOneAndUpdate({
    _id: id,
    created_by: userId,
  }, {$set: {status: 'POSTED'}});

  const truck = await Truck.find({
    status: 'IS',
    assigned_to: {$ne: null},
  });

  if (load.payload < truck.payload && load.dimensions
      .length < truck.dimensions.length && load.dimensions
      .height < truck.dimensions.height) {
    load.status = 'ASSIGNED';
    load.state = 'En route to Pick Up';
    load.logs.push({
      message: `Load assigned to driver with id ${truck.assigned_to}`,
      time: new Date(Date.now()),
    });
    truck.status = 'OL';
    await load.save();
    await truck.save();
  }
  if (load.assigned_to) {
    return {
      message: 'Load posted successfully',
      driver_found: true,
    };
  }

  if (!load.assigned_to) {
    return {
      message: 'To truck is available',
      driver_found: false,
    };
  }
};

const getLoadShippingInfoById = async (id, userId) => {
  const load = await Load.findOne({
    _id: id,
    created_by: userId,
  });
  const truck = await Truck.findOne({
    assigned_to: load.assigned_to,
  });

  return {'load': load, 'truck': truck};
};

module.exports = {
  getUserLoads,
  addLoad,
  getActiveLoads,
  setLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
};
