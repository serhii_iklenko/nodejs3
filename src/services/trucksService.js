const {Truck} = require('../models/truckModel');
const {InvalidRequestError} = require('../utils/errors');

const getTrucks = async (userId) => {
  const trucks = await Truck.find({created_by: userId}, '-__v');
  return trucks;
};

const addTruck = async (driverId, truckPayload) => {
  const truck = new Truck({...truckPayload, driverId});
  await truck.save();
};

const getTruckById = async (truckId, driverId) => {
  const truck = await Truck.findOne({_id: truckId,
    created_by: driverId}, '-__v');
  return truck;
};

const updateTruckById = async (truckId, driverId, type) => {
  const truck = Truck.findOne({_id: truckId, created_by: driverId});
  await truck.updateOne({$set: {type}});
};

const deleteTruckById = async (truckId, driverId) => {
  const truck = Truck.findOne({_id: truckId, created_by: driverId});
  await truck.remove();
};

const assignTruckById = async (truckId, driverId) => {
  const assignedTruck = await Truck.findOne({created_by: driverId,
    assigned_to: {$ne: null}});
  if (assignedTruck) {
    throw new InvalidRequestError('You have already assigned truck!');
  }

  const truck = await Truck.findOneAndUpdate({_id: truckId,
    assigned_to: null}, {$set: {assigned_to: driverId}});
  return truck;
};

const unassignTruckById = async (truckId, driverId) => {
  const truck = await Truck.findOneAndUpdate({_id: truckId, driverId},
      {$set: {assigned_to: null}});
  console.log(truck);
  return truck;
};

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
  unassignTruckById,
};
