const Joi = require('joi');
const {InvalidRoleError, InvalidTypeError} = require('../utils/errors');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string()
        .min(6)
        .max(20)
        .required(),
    email: Joi.string()
        .email()
        .required(),
    role: Joi.string()
        .valid('DRIVER', 'SHIPPER')
        .error(new InvalidRoleError('Only DRIVER ' +
            'and SHIPPER roles are allowed'))
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(new Error(err.message));
  }
};

const truckValidator = async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
        .error(new InvalidTypeError('Only SPRINTER, SMALL STRAIGHT ' +
            'and LARGE STRAIGHT truck types are allowed'))
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(new Error(err.message));
  }
};

module.exports = {
  registrationValidator, truckValidator,
};
