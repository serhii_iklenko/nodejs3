const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: 'IS',
  },
  created_by: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  assigned_to: {
    type: mongoose.Schema.Types.ObjectId,
    default: null,
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Truck};
