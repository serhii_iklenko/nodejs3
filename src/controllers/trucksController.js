const express = require('express');
const router = new express.Router();

const {
  addTruck,
  getTrucks,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
  unassignTruckById,
} = require('../services/trucksService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

const {truckValidator} = require('../middlewares/validationMidlleware');


router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const trucks = await getTrucks(userId);
  res.json({trucks});
}));

router.post('/', truckValidator, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const truck = {
    type: req.body.type,
    created_by: userId,
  };

  await addTruck(userId, truck);
  res.json({message: 'Truck created successfully'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;


  const truck = await getTruckById(id, userId);
  res.json({truck});
}));

router.put('/:id', truckValidator, asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {type} = req.body;

  await updateTruckById(id, userId, type);
  res.json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteTruckById(id, userId);
  res.json({message: 'Truck deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await assignTruckById(id, userId);
  res.json({message: 'Truck assigned successfully'});
}));

router.post('/:id/unassign', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await unassignTruckById(id, userId);

  res.json({message: 'Truck unassigned successfully'});
}));

module.exports = {
  truckRouter: router,
};
