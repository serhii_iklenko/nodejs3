const express = require('express');
const router = new express.Router();

const {
  register,
  login,
  forgotPassword,
} = require('../services/authService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

const {registrationValidator} = require('../middlewares/validationMidlleware');

router.post('/register', registrationValidator,
    asyncWrapper(async (req, res) => {
      const {
        email,
        password,
        role,
      } = req.body;

      await register({email, password, role});

      res.status(200).json({message: 'Profile created successfully'});
    }));

router.post('/login', asyncWrapper(async (req, res) => {
  const {
    email,
    password,
  } = req.body;

  const token = await login({email, password});

  res.status(200).json({
    message: 'Success',
    jwt_token: token,
  });
}));

router.post('/forgot_password', asyncWrapper(async (req, res) => {
  const {
    email,
  } = req.body;

  await forgotPassword({email});

  res.status(200).json({message: 'New password sent to your email address'});
}));

module.exports = {
  authRouter: router,
};
