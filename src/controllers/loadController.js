const express = require('express');
const router = new express.Router();

const {InvalidRequestError} = require('../utils/errors');

const {
  getUserLoads,
  addLoad,
  getActiveLoads,
  setLoadState,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postLoadById,
  getLoadShippingInfoById,
} = require('../services/loadService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const status = req.query.status;
  const loads = await getUserLoads(userId, status);

  res.json({loads});
}));

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const data = req.body;
  const {role} = req.user;

  if (role == 'DRIVER') {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    await addLoad(userId, data);
    res.json({'message': 'Load created successfully'});
  }
}));

router.get('/active', asyncWrapper( async (req, res) => {
  const { userId, role } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const load = await getActiveLoads(userId)
    res.status(200).json({ load })
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
  const { userId, role } = req.user

  if (role !== 'DRIVER') {
    throw new InvalidRequestError('Access denied')
  }

  try {
    const load = await getActiveLoads(userId)
    if (!load) {
      throw new InvalidRequestError('No load found')
    } else {
      let state = load.state
      const states = [null, 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
      let index = states.indexOf(state)
      if (index === states.length - 1) {
        throw new InvalidRequestError(`You can't change state '${state}'`)
      } else {
        state = states[index + 1]
      }
      await setLoadState(userId, state)
      res.status(200).json({ message: `Load state changed to '${state}'` })
    }
  } catch (err) {
    throw new InvalidRequestError(err.message)
  }
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  if (!load) {
    throw new InvalidRequestError('No load with such id found!');
  }

  const load = await getLoadById(id, userId);
  res.json({load});
}));

router.put('/:id', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const data = req.body;
  const {role} = req.user;

  if (role == 'DRIVER') {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    await updateLoadById(id, userId, data);
    res.json({'message': 'Load details changed successfully'});
  }
}));

router.delete('/:id', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;

  await deleteLoadById(id, userId);

  res.json({'message': 'Load deleted successfully'});
}));

router.post('/:id/post', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {role} = req.user;

  if (role == 'DRIVER') {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    const done = await postLoadById(id, userId);
    res.json({
      'message': 'Load posted successfully',
      'driver_found': done,
    });
  }
}));

router.get('/:id/shipping_info', asyncWrapper( async (req, res) => {
  const {userId} = req.user;
  const {id} = req.params;
  const {role} = req.user;

  if (role == 'DRIVER') {
    throw new InvalidRequestError('Not available for drivers');
  } else {
    const shippingInfo = await getLoadShippingInfoById(id, userId);
    res.json({shippingInfo});
  }
}));

module.exports = {
  loadRouter: router,
};
